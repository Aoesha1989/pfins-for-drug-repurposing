/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfinnetwork;
import java.util.*;
import java.io.*;
/**
 *
 * @author aoeshagaedmalsobhe
 */
public class Mapping {
    public Map<String,String> MappedDiseases(Set<String>Diseases,Map<String,Set<String>>Cluster)throws IOException{
     Map<String,String> Symbol= new HashMap<String,String>();
     Map<String,Set<String>> ClusterMapping= new HashMap<String,Set<String>>();
    Set<String>set=new HashSet<String>();
     Set<String>set2=new HashSet<String>();
     String filename = "diseasesattributes.tsv";
     File bioFile = new File(filename);
        BufferedReader in = new BufferedReader(new FileReader(bioFile));
        in.readLine();
        String line;
        Map<String,String>Mapping=new HashMap<String,String>();
        while ((line = in.readLine()) != null) {
            String[] colums = line.split("\t");
            String ID = colums[1];
            String Name = colums[2];
            Mapping.put(ID,Name);
            set.add(Name);
            set2.add(ID);
}               
            //System.out.print(Mapping);
           //System.out.print(Nodes.size()+"\n");
           //System.out.print(set.size()+"\n");
           //System.out.print(set2.size()+"\n");
            for(String s: Diseases){
                if(Mapping.containsKey(s)){
        
                 Symbol.put(s,Mapping.get(s));
        
        }
          } 
            //System.out.print(Symbol.size());

          /* BufferedWriter out = null;
         try {
         
         out = new BufferedWriter(new FileWriter(filename));
         for(String s : Symbol.keySet()){
         out.write(s + " \t" + Symbol.get(s) + "\n");}
         }
         catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }*/
                    for(String i : Cluster.keySet()){
                          Set<String> set1 =Cluster.get(i);
                          Set<String> mappedset = new HashSet<String>();
                          for(String j : set1){
                          
                             if(Mapping.containsKey(j)){
        
                               mappedset.add(Mapping.get(j));
                          
                          }
                      }
                      ClusterMapping.put(i,mappedset);
                      
                      }
        PrintWriter bw10 = null;
        try {
                  
            
                 bw10 = new PrintWriter(new FileWriter(new File("ClusterMapping.txt")));
                      for(String i : ClusterMapping.keySet()){
                   bw10.println(i + "\t"+ ClusterMapping.get(i));
                      }
                   bw10.close();
                              }
        catch(Exception e){
           e.printStackTrace();

                           }   
                
            finally {
            if (bw10 != null) {
                try {
                    bw10.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
         



         
      
         return Symbol;
}

public Map<String,String> MappedGenes(Set<String>Genes)throws IOException{
     Map<String,String> Symbol2= new HashMap<String,String>();
    Set<String>set=new HashSet<String>();
     Set<String>set2=new HashSet<String>();
     String filename = "genesattribute.tsv";
     File bioFile = new File(filename);
        BufferedReader in = new BufferedReader(new FileReader(bioFile));
        in.readLine();
        String line;
        Map<String,String>Mapping=new HashMap<String,String>();
        while ((line = in.readLine()) != null) {
            String[] colums = line.split("\t");
            String ID = colums[0];
            String Name = colums[2];
            Mapping.put(ID,Name);
            //set.add(Name);
            //set2.add(ID);
}               
            //System.out.print(Mapping);
           //System.out.print(Nodes.size()+"\n");
           //System.out.print(set.size()+"\n");
           //System.out.print(set2.size()+"\n");
            for(String s: Genes){
                if(Mapping.containsKey(s)){
        
                 Symbol2.put(s,Mapping.get(s));
        
        }
          } 
          return Symbol2;
}
}