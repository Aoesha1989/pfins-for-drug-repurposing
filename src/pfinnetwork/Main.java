
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pfinnetwork;

import GoldStandard.goldstandard;
import GoldStandard.goldstandardparsing;
import Integration.IntegratedScores;
import Integration.*;
import LLS.LTPSCores;
import LLS.*;
import Prediction.NetworkPrediction;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import Prediction.*;
import java.util.*;


/**
 *
 * @author aoeshagaedmalsobhe
 */
public class Main {
    public static void main(String[] args) throws IOException {
      
   String filename = "allgenediseasepmidassociations.tsv";
        PFINNetWork PFIN = new PFINNetWork();
        ParsingBioGrid P = PFIN.parsedata(filename);
        BuildingBioGrid B = PFIN.buildbiodata(P);
        PFIN.writetoFile(B) ;
       PFIN.writetoFile2(B);
       goldstandardparsing gs = new goldstandardparsing();
        goldstandard gss =gs.ParsingGS(B.getGS());
         Score S = new Score();
         LTPSCores LTP = new LTPSCores();
           Map<String,Double>LLSScore=S.ScoredData(gss, B);
            Map<String,Double>LLSScore2=LTP.LTPScore(B);
            LLSScore.putAll(LLSScore2);
        IntegratedScores IS = new IntegratedScores();
          Map<Pair,Double>IntegratedS=IS.doIntegration(LLSScore,LLSScore,Collections.<Double>reverseOrder(),B);
           System.out.print("size of network"+"\t"+IntegratedS.size()+"\n");
           Set<String>genes=new HashSet<String>();
           Set<String>diseases=new HashSet<String>();
           for(Pair p:IntegratedS.keySet()){
              genes.add(p.getG());
              diseases.add(p.getD());
           }
           System.out.print("genes"+genes.size()+"\n"+"diseases"+"\t"+diseases.size()+"\n");
            Integrationlogger ILogger = new Integrationlogger();
         ILogger.logToFile("integrated.txt",IntegratedS);
         
          /* DValue Dvalue = new DValue();
             Dvalue.run(gss);
             NodesDistribution N= new NodesDistribution();
             N.NodesDistribution1();
              N.NodesDistribution2();
              ClusterAnalysis Cl = new ClusterAnalysis();
              Map<String, Set<String>>Cluster=Cl.clusters();
              Cl.run(IntegratedS,Cluster);*/
         Map<Pair,Set<List>>AUCAvarege=new HashMap<Pair,Set<List>>();
          Map<Pair,Double>Network=new HashMap<Pair,Double>();
           Network.putAll(IntegratedS);
           /*for(Pair P1 : IntegratedS.keySet()){
               if(IntegratedS.get(P1)<15){
                  Network.remove(P1);
               }
           
           }*/
              System.out.print("Size Network after removing"+"\t"+Network.size()+"\n");
         
        List<Pair>network=new ArrayList<Pair>();
         List<Map>networks=new ArrayList<Map>();
         Set<Double>d=new HashSet<Double>();
         Set<Double>d1=new HashSet<Double>();

          for(Pair link:Network.keySet()){
              network.add(link);
         
         }
         System.out.print("size list"+"\t"+network.size()+"\n");
         for( int i =0; i<10;i++){
             
               
          Map<Pair,Double>subnetwork=new HashMap<Pair,Double>();
            while(subnetwork.size()<2556) {
                            
                Random random = new Random();
                int randomNumber = random.nextInt(network.size());
                subnetwork.put(network.get(randomNumber),Network.get(network.get(randomNumber)));
                //System.out.print(network.get(randomNumber)+"\t"+Network.get(network.get(randomNumber))+"\n"+"\n");
                
                network.remove(network.get(randomNumber));
               
            }
             networks.add(subnetwork);
         }
           for(int s=0;s<4;s++){
               networks.get(0).put(network.get(s), Network.get(network.get(s)));
           }
         /*Map<Pair,Double>Train=new HashMap<Pair,Double>(); 
           for(int j=0;j<9;j++){
               Train.putAll(networks.get(j));
           }*/
         //System.out.print(networks.get(9)+"\n"+"\n");  
                //System.out.print(networks.get(8)+"\n"+"\n");  
                 for(int j=0;j<10;j++){
                      List<Map>newList=new ArrayList<Map>();
                      newList.addAll(networks);
                   Map<Pair,Double>Train=new HashMap<Pair,Double>();
                     Map<Pair,Double>Test=new HashMap<Pair,Double>();
                       Test.putAll(newList.get(j));
                        newList.remove(j);
                        for(int s=0;s<9;s++){
                        Train.putAll(newList.get(s));
                        
                        }
                        System.out.print(Test);
         NetworkPrediction NP = new NetworkPrediction();
        
       Map<String,Set<Pair>>GeneNeighbour=NP.GNeighbours(Train);
       
        Map<String,Set<String>>DNeighbour=NP.DNeighbours(Train);
       
        Map<Pair,Set<String>>Common=NP.CommonNeighbours(Train,DNeighbour, GeneNeighbour);
        System.out.print(Common.size());
       Map<Pair,Double>Predicatedassociation=NP.Predict(Train,GeneNeighbour,DNeighbour,Common);
         NP.writetoFilePrediction(Predicatedassociation);
   
         Validation vd = new Validation();
         Set<List>AUC=vd.NetworkValidat(Test,j);
        
         }
         
           
        
                          
       
           
         
      /* 
                 
             
             ListDiseaseGenes LGD= Cl.AnalysisCL();
             
         Map<String, Set<String>> genesList=Cl.listofGenes(LGD);
        DiseaseNetwork DN = new DiseaseNetwork();
      Map<Pair,Set<Pair>>Evidence=DN.DiseaseNet(B);
      System.out.print(Evidence.size()+"\n");
        Map<Pair, Double>FinalDD=DN.doIntegration(LLSScore,LLSScore,Collections.<Double>reverseOrder(),B,Evidence);
       Map<Pair, List<String>>FinalASSO=DN.doIntegrationasso(B,Evidence);
       ILogger.logToFile("integratedD-DPFINS.txt",FinalDD);
     
      /* Cl.run(genesList,Cluster);
     NodesDistribution N= new NodesDistribution();
        N.NodesDistribution1();
         N.NodesDistribution2();
            
          */
   
   
}
}